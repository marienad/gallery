package com.example.gallery;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import java.io.File;
import java.util.ArrayList;

public class ImageAdapter extends PagerAdapter {
    private ArrayList<String> galleryList;
    private Context context;
    private int position;

    ImageAdapter(Context context, ArrayList<String> galleryList, int position) {
        this.context = context;
        this.galleryList = galleryList;
        this.position = position;
    }

    @Override
    public int getCount() {
        return galleryList.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        position += this.position;
        if (position == galleryList.size()) {
            position = 0;
            this.position=0;
        }
        ImageView imageView = new ImageView(context);
        setImageFromPath(galleryList.get(position), imageView);
        container.addView(imageView, 0);
        return imageView;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((ImageView) object);
    }

    private void setImageFromPath(String path, ImageView image) {
        File imgFile = new File(path);
        if (imgFile.exists()) {
            Bitmap myBitmap = ImageHelper.decodeSampleBitmapFromPath(imgFile.getAbsolutePath(), 200, 200);
            image.setImageBitmap(myBitmap);
            image.setScaleType(ImageView.ScaleType.FIT_CENTER);
        }
    }
}
