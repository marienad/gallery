package com.example.gallery;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

public class ImageActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image);

        ViewPager viewPager = findViewById(R.id.galleryView);
        ImageAdapter imageAdapter = new ImageAdapter(getApplicationContext(), getIntent().getStringArrayListExtra("image"), getIntent().getIntExtra("number",0));
        viewPager.setAdapter(imageAdapter);
    }
}
